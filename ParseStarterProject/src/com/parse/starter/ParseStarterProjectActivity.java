package com.parse.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class ParseStarterProjectActivity extends Activity {
	/** Called when the activity is first created. */
	private Button fbLogin;
	private Activity currentActivity;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentActivity = this;
		Parse.initialize(this, "m6zTkVhDB73TaEjClsMeT6TID7l3IqxUu9UzgBoJ", "36VxdP32ethLMGAIy3gpwq60rBMHYU17IfxukHqN");
		ParseFacebookUtils.initialize("1461246090758663");
		/*
		ParseObject testObject = new ParseObject("TestObject");
		testObject.put("foo", "bar");
		testObject.saveInBackground();
		*/
		setContentView(R.layout.main);
		ParseAnalytics.trackAppOpened(getIntent());
		
		fbLogin = (Button) findViewById(R.id.button1);
		fbLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				  ParseFacebookUtils.logIn(currentActivity, new LogInCallback() {
				  @Override
				  public void done(ParseUser user, ParseException err) {
				    if (user == null) {
				      Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
				    } else if (user.isNew()) {
				      Log.d("MyApp", "User signed up and logged in through Facebook!");
				    } else {
				      Log.d("MyApp", "User logged in through Facebook!");
				    }
				  }
				});
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
}
