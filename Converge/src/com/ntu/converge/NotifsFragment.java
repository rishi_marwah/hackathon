package com.ntu.converge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;

public class NotifsFragment extends ListFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_feed, container,
				false);

		ArrayList<Map<String, String>> list = buildData();
	    String[] from = { "text"};
	    int[] to = { R.id.text };

	    SimpleAdapter adapter = new SimpleAdapter(inflater.getContext() , list,
	        R.layout.notifs_item, from, to);
	    setListAdapter(adapter);
		return rootView;
	}

	private ArrayList<Map<String, String>> buildData() {
		ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
		list.add(putData("You have been invited to XXXX at 1:30 by YYYY "));
		list.add(putData("You have been invited to XXXX at 1:30 by YYYY "));
		list.add(putData("You have been invited to XXXX at 1:30 by YYYY "));
		return list;
	}

	private HashMap<String, String> putData(String text) {
		HashMap<String, String> item = new HashMap<String, String>();
		item.put("text", text);
		return item;
	}

}