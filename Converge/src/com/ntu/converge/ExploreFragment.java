package com.ntu.converge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;

public class ExploreFragment extends ListFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_feed, container,
				false);

		ArrayList<Map<String, String>> list = buildData();
	    String[] from = { "content", "creator", "time"};
	    int[] to = { R.id.content, R.id.creator, R.id.time };

	    SimpleAdapter adapter = new SimpleAdapter(inflater.getContext() , list,
	        R.layout.feed_item, from, to);
	    setListAdapter(adapter);
		return rootView;
	}

	private ArrayList<Map<String, String>> buildData() {
		ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
		list.add(putData("Android", "Mobile", "1300"));
		list.add(putData("Windows7", "Windows7", "1400"));
		list.add(putData("iPhone", "iPhone", "1600"));
		list.add(putData("Android", "Mobile", "1300"));
		list.add(putData("Windows7", "Windows7", "1400"));
		list.add(putData("iPhone", "iPhone", "1600"));
		list.add(putData("Android", "Mobile", "1300"));
		list.add(putData("Windows7", "Windows7", "1400"));
		list.add(putData("iPhone", "iPhone", "1600"));
		list.add(putData("Android", "Mobile", "1300"));
		list.add(putData("Windows7", "Windows7", "1400"));
		list.add(putData("iPhone", "iPhone", "1600"));
		list.add(putData("Android", "Mobile", "1300"));
		list.add(putData("Windows7", "Windows7", "1400"));
		list.add(putData("iPhone", "iPhone", "1600"));
		list.add(putData("Android", "Mobile", "1300"));
		list.add(putData("Windows7", "Windows7", "1400"));
		list.add(putData("iPhone", "iPhone", "1600"));
		list.add(putData("Android", "Mobile", "1300"));
		list.add(putData("Windows7", "Windows7", "1400"));
		list.add(putData("iPhone", "iPhone", "1600"));
		
		return list;
	}

	private HashMap<String, String> putData(String content, String creator, String time) {
		HashMap<String, String> item = new HashMap<String, String>();
		item.put("content", content);
		item.put("creator", creator);
		item.put("time", time);
		return item;
	}

}