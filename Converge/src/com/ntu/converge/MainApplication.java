package com.ntu.converge;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

public class MainApplication extends Application {

	static final String TAG = "Converge";
	
	@Override
	public void onCreate() {
		super.onCreate();

		// Add your initialization code here
		Parse.initialize(this, "m6zTkVhDB73TaEjClsMeT6TID7l3IqxUu9UzgBoJ", "36VxdP32ethLMGAIy3gpwq60rBMHYU17IfxukHqN");
		ParseFacebookUtils.initialize("1461246090758663");
		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
	    
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		
		ParseACL.setDefaultACL(defaultACL, true);
	}

}
