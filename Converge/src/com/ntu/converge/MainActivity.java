package com.ntu.converge;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class MainActivity extends Activity {
	/** Called when the activity is first created. */
	private LoginButton fbLogin;
	private Dialog progressDialog;

	private static String userID;
	
	public static String getUserID() {
		return userID;
	}
	//private Activity currentActivity;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//currentActivity = this;
		Parse.initialize(this, "m6zTkVhDB73TaEjClsMeT6TID7l3IqxUu9UzgBoJ", "36VxdP32ethLMGAIy3gpwq60rBMHYU17IfxukHqN");
		ParseFacebookUtils.initialize("1461246090758663");
		
		setContentView(R.layout.main);
		ParseAnalytics.trackAppOpened(getIntent());
		
		fbLogin = (LoginButton) findViewById(R.id.button1);
		fbLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onLoginButtonClicked();
			}
		});
		
//		Session session = ParseFacebookUtils.getSession();
//		if (session != null && session.isOpened()) {
//			session.closeAndClearTokenInformation();
//		}
//		
//		ParseUser.logOut();
		
		//Check if there is a currently logged in user
		// and they are linked to a Facebook account.

		ParseUser currentUser = ParseUser.getCurrentUser();
		if ((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) {
			// Go to the user info activity
			getUserId();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}
	
	private void onLoginButtonClicked() {
		MainActivity.this.progressDialog = ProgressDialog.show(
				MainActivity.this, "", "Logging in...", true);
	    List<String> permissions = Arrays.asList("basic_info", "user_about_me",
	           "user_friends", "user_location");
	    ParseFacebookUtils.logIn(permissions, MainActivity.this, new LogInCallback() {
	        @Override
	        public void done(ParseUser user, ParseException err) {
	        	MainActivity.this.progressDialog.dismiss();
	            if (user == null) {
	                Log.d(MainApplication.TAG,"Uh oh. The user cancelled the Facebook login.");

	            } else if (user.isNew()) {
	                Log.d(MainApplication.TAG,"User signed up and logged in through Facebook!");
	                setUserDetails(user,err);
	                getUserId();

	            } else {
	                Log.d(MainApplication.TAG,"User logged in through Facebook!");
	                getUserId();
	            }
	            
	        }
	    });
	}
	
	private void getUserId(){

		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
				new Request.GraphUserCallback() {
			@Override
			public void onCompleted(GraphUser user, Response response) {
				if (user != null) { 
					MainActivity.userID = user.getId();
//					updateInterest();
					Intent intent  = new Intent(MainActivity.this, HomeActivity.class);
					startActivity(intent);
					}  else if (response.getError() != null) {
						// handle error
					}                  
				}               
			});
			request.executeAsync();//executeAsync();

		}
	private void setUserDetails(ParseObject user, ParseException err){

		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
				new Request.GraphUserCallback() {
			@Override
			public void onCompleted(final GraphUser user, Response response) {
				if (user != null) { 
					
					ParseObject userProfile = new ParseObject("User_data");
					try {                   
						// Populate the JSON object 
						//ParseUser curr = ParseUser.getCurrentUser();
						//urr.put("fb_id",user.getId());
						userProfile.put("facebookId", user.getId());
						userProfile.put("name", user.getName());
						if (user.getLocation().getProperty("name") != null) {
							userProfile.put("location", (String) user
									.getLocation().getProperty("name"));    
						}     
						
						MainActivity.this.userID = user.getId();
						
						if (user.getProperty("gender") != null) {
							userProfile.put("gender",       
									(String) user.getProperty("gender"));   
						}                           
						if (user.getBirthday() != null) {
							userProfile.put("birthday",     
									user.getBirthday());                    
						}      
						
						Request.newMyFriendsRequest(ParseFacebookUtils.getSession(), new Request.GraphUserListCallback() {

					        @Override
					        public void onCompleted(List<GraphUser> users, Response response) {
					            if (response.getError() == null) {
					            	
					            	for(GraphUser usr : users){		
					            		ParseObject frndlist = new ParseObject("Friends");
					            		//frndlist.addUnique(user.getFirstName()+user.getId(),(String)usr.getId());
					            		frndlist.put("user_id", user.getId());
					            		frndlist.put("friend_id", usr.getId());
					            		frndlist.saveInBackground();
					            	}
					                // Handle response
					            }

					        }
						} 
						).executeAsync();
						
		

						
						userProfile.saveInBackground();
						
						
						// Now add the data to the UI elements
						// ...

					} catch (Exception e) {
						Log.d(MainApplication.TAG,
								"Error parsing returned user data.");
					}

				} else if (response.getError() != null) {
					// handle error
				}                  
			}               
		});
		request.executeAsync();

	}

//	private void updateInterest(){
//		Intent intent  = new Intent(this, UpdateInterest.class);
//		startActivity(intent);
//	}
}
