package com.ntu.converge;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class UpdateInterest extends Activity {
	private LinearLayout ll;
	
	@Override	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final String userid = MainActivity.getUserID();
		
		ll = new LinearLayout(this);
		
		final ArrayList<Integer> currentInterests = new ArrayList<Integer>();
		//final ArrayList<String> currentInterestsObjs = new ArrayList<String>();
		ParseQuery<ParseObject> queryUserInterest = ParseQuery.getQuery("UserInterests");
		queryUserInterest.whereEqualTo("user_id", userid);
		List<ParseObject> userInterests = null;
		try {
			userInterests = queryUserInterest.find();
			for(ParseObject interests : userInterests){
				currentInterests.add(interests.getInt("interest_id"));
				//currentInterestsObjs.add(interests.getObjectId());
			}
			
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		final ArrayList<String> allInterestsNames = new ArrayList<String>();
		final ArrayList<Integer> allInterestsIds = new ArrayList<Integer>();
		ParseQuery<ParseObject> queryAllInterest = ParseQuery.getQuery("Interests");
		List<ParseObject> Interests;
		try {
			Interests = queryAllInterest.find();
			for(ParseObject interests : Interests){
				Log.d("Tag",interests.getString("name")+"bhj,");
				allInterestsNames.add(interests.getString("name"));
				Log.d("Tag",interests.getInt("interest_id")+"bhj,");
				allInterestsIds.add(interests.getInt("interest_id"));
				
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		ScrollView sv = new ScrollView(this);
		ll.setId(55);
		ll.setOrientation(LinearLayout.VERTICAL);

		Log.d("all interests",allInterestsNames.size()+"");
		
		for(int i = 0; i < allInterestsNames.size(); i++) {
			CheckBox cb = new CheckBox(this);
			int interestId = allInterestsIds.get(i);
			cb.setText(allInterestsNames.get(i));
			cb.setId(interestId);
			cb.setTextSize(30);
			Log.d("checkbox", "cb id : " + cb.getId());
			if(currentInterests.contains(interestId))
				cb.setChecked(true);
			ll.addView(cb);
		}

		Button b = new Button(this);
		b.setText("Update");
		ll.addView(b);
		sv.addView(ll);
		
		
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ArrayList<Integer> interestUpdates = new ArrayList<Integer>();
				for(int i = 0; i < allInterestsIds.size(); i++) {
					
					CheckBox cb = (CheckBox) ll.findViewById(allInterestsIds.get(i));
					if(cb.isChecked()){
						
						interestUpdates.add(allInterestsIds.get(i));
					}
						//Log.d("is checked ", cb.getId() + " ");
					}
				onUpdateButtonClicked(interestUpdates,userid);
			}
		});
		
		this.setContentView(sv);

	}

	//onclick set checked and update

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_interest, menu);
		return true;
	}
	
	private void onUpdateButtonClicked(ArrayList<Integer> updates,String userid){
		
		ParseQuery<ParseObject> queryUserInterest = ParseQuery.getQuery("UserInterests");
		queryUserInterest.whereEqualTo("user_id", userid);
		List<ParseObject> userInterests = null;
		try {
			userInterests = queryUserInterest.find();
			ParseObject.deleteAllInBackground(userInterests, null);//deleteAll(userInterests);
			
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for(int interest_id : updates){
			ParseObject interestUpdates = new ParseObject("UserInterests");
			Log.d("chc",userid + ", " +interest_id);
			interestUpdates.put("user_id",userid);
			interestUpdates.put("interest_id",interest_id);
			interestUpdates.saveInBackground();
		}
		Intent intent  = new Intent(UpdateInterest.this, HomeActivity.class);
		startActivity(intent);

	}
}
